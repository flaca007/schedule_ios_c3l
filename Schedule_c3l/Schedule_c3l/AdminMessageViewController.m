//
//  AdminMessageViewController.m
//  Schedule
//
//  Created by susan on 10/22/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import "AdminMessageViewController.h"
#import "User.h"
#import "Message.h"
#import "UserStore.h"
#import "AdminStore.h"
@interface AdminMessageViewController ()

@end

@implementation AdminMessageViewController
{
    UITextField *activeField;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    if([[self sendList]count] > 1){
        [[self toLabel]setText:@"All Students"];
    }else{
        User *user = [[self sendList]objectAtIndex:0];
        [[self toLabel]setText:[NSString stringWithFormat:@"%@ %@", user.userName, user.lastName]];
    }
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sendMessage:(id)sender
{
    NSDate *date = [[NSDate alloc]init];

    Message *message = [[Message alloc]initMessageWithTitle:[[self titelTextField]text] sentDate:date content:[[self messageTextView]text] createdBy:[NSString stringWithFormat:@"%@ %@", [[[UserStore sharedStore]activeUser]userName],[[[UserStore sharedStore]activeUser]lastName]] db_id:@"" db_rev:@"" type:@"message"];

    for(User *student in [self sendList]){
        [message addStudent:student.db_id];
    }

    [[AdminStore sharedStore]sendMessage:message callback:^(NSError *error) {
        //
        if(!error){
            [[self navigationController]popViewControllerAnimated:YES];
            NSLog(@"message: %@", message);
        }/*else{
            self.selectedEvent = self.theOldEvent;
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"ALERT" message:@"It did not go well" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            
        }*/
    }];
    
    
}



// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-kbSize.height);
        [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}

@end
