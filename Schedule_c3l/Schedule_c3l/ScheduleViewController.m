//
//  ScheduleViewController.m
//  Schedule
//
//  Created by susan on 10/10/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import "ScheduleViewController.h"
#import "UserStore.h"
#import "AdminTableViewController.h"
@interface ScheduleViewController ()

@end

@implementation ScheduleViewController

//-(void)viewDidLoad
//{
//    [super viewDidLoad];
//    self.navigationController.toolbarHidden = false;
//}

- (IBAction)login:(id)sender
{
    if((self.userNameText.text.length == 0)){
        NSLog(@"wrong: %@", self.userNameText.text);
    }else{
        //NSLog(@"right: %@", self.userNameText.text);
        
        [[UserStore sharedStore] logInUser:self.userNameText.text callback:^{
            
            User *user = [[UserStore sharedStore]activeUser];
            //NSLog(@"user from mainview: %@", user);
            if([[UserStore sharedStore]activeUser]){
                if([[user userRole] isEqualToString:@"Student"]){
                    NSLog(@"I'm a student segue me to my storyboard");
                    
                    [self performSelectorOnMainThread:@selector(goToStudentHomePage) withObject:nil waitUntilDone:YES];                    
                    
                }else{
                    //get all users, get all courses for admin
                    NSLog(@"Im the boss");
                    [self performSelectorOnMainThread:@selector(goToAdminHomePage) withObject:nil waitUntilDone:YES];
                }
            }
        }];
        
    }
}

- (IBAction)adminLogin:(id)sender
{
    [[UserStore sharedStore] logInUser:@"flaca007@gmail.com" callback:^{
        
        User *user = [[UserStore sharedStore]activeUser];
        //NSLog(@"user from mainview: %@", user);
        if([[UserStore sharedStore]activeUser]){
            if([[user userRole] isEqualToString:@"Student"]){
                NSLog(@"I'm a student segue me to my storyboard");
                
                [self performSelectorOnMainThread:@selector(goToStudentHomePage) withObject:nil waitUntilDone:YES];
                
            }else{
                //get all users, get all courses for admin
                NSLog(@"Im the boss");
                [self performSelectorOnMainThread:@selector(goToAdminHomePage) withObject:nil waitUntilDone:YES];
            }
        }
    }];
}

- (IBAction)studentLogin:(id)sender
{
    [[UserStore sharedStore] logInUser:@"apontnet@gmail.com" callback:^{
        
        User *user = [[UserStore sharedStore]activeUser];
        //NSLog(@"user from mainview: %@", user);
        if([[UserStore sharedStore]activeUser]){
            if([[user userRole] isEqualToString:@"Student"]){
                NSLog(@"I'm a student segue me to my storyboard");
                
                [self performSelectorOnMainThread:@selector(goToStudentHomePage) withObject:nil waitUntilDone:YES];
                
            }else{
                //get all users, get all courses for admin
                NSLog(@"Im the boss");
                [self performSelectorOnMainThread:@selector(goToAdminHomePage) withObject:nil waitUntilDone:YES];
            }
        }
    }];
}
-(void)goToStudentHomePage
{
     [self performSegueWithIdentifier:@"toStudentHomePage" sender:self];
}
-(void)goToAdminHomePage


{
    [self performSegueWithIdentifier:@"adminTest" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"adminTest"]){
        AdminTableViewController *adminTableViewController = [segue destinationViewController];
        [adminTableViewController setIsLoaded:@"NO"];
    }

}
@end
