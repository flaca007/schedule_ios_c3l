//
//  Utilities.m
//  Schedule
//
//  Created by susan on 10/25/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import "Utilities.h"

@implementation Utilities


-(NSDate *)dateWithRightTimeZone
{
    NSDate* sourceDate = [NSDate date];
    
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    NSDate* now = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
    return now;
}
-(NSDateFormatter *)dateFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    return formatter;
}
-(NSDateFormatter *)dateFormatterWithTime
{
    NSDateFormatter *formatter = [self dateFormatter];
    [formatter setDateFormat:@"HH:mm"];

    return formatter;
}
-(NSDateFormatter *)dateFormatterWithDateTime
{
    NSDateFormatter *formatter = [self dateFormatter];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    return formatter;
}
-(NSDateFormatter *)dateFormatterWithDate
{
    NSDateFormatter *formatter = [self dateFormatter];
    [formatter setDateFormat:@"dd MMMM"];
    
    return formatter;
}
@end
