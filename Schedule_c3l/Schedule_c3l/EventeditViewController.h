//
//  EventeditViewController.h
//  Schedule_c3l
//
//  Created by susan on 10/31/12.
//  Copyright (c) 2012 Susan Sarabia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Course.h"
#import "CourseEvent.h"
@interface EventeditViewController : UIViewController
@property(nonatomic, strong)Course *selectedCourse;
@property(nonatomic, strong)CourseEvent *selectedEvent;
@property(nonatomic, strong)CourseEvent *theOldEvent;

@property( nonatomic) BOOL isStartDateSelected;

@property(nonatomic)BOOL isNew;

@property (weak, nonatomic) IBOutlet UITextField *eventClassroom;
@property (weak, nonatomic) IBOutlet UITextField *eventAltTeacher;
@property (weak, nonatomic) IBOutlet UITextView *eventReadingInstructions;
@property (weak, nonatomic) IBOutlet UITextField *eventDescription;

@property (weak, nonatomic) IBOutlet UIButton *startDate;
@property (weak, nonatomic) IBOutlet UIButton *endDate;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

- (IBAction)save:(id)sender;

- (IBAction)editStartdate:(id)sender;
- (IBAction)editEndDate:(id)sender;

@end
