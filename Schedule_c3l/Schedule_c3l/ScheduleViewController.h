//
//  ScheduleViewController.h
//  Schedule
//
//  Created by susan on 10/10/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScheduleViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *userNameText;
- (IBAction)login:(id)sender;
- (IBAction)adminLogin:(id)sender;
- (IBAction)studentLogin:(id)sender;

@end
