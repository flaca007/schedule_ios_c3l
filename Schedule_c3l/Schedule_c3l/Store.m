//
//  Store.m
//  Schedule
//
//  Created by susan on 10/15/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import "Store.h"
#import "ConnectionToDatabase.h"
@implementation Store

-(void)fetchData:(NSString *)idOfSomeAddres block:(void (^)(NSDictionary *dictionary, NSError *err))block
{
    NSString *newurl = [NSString stringWithFormat:@"%@%@", BASE_URL, idOfSomeAddres];
    NSURL *url = [NSURL URLWithString:newurl];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    
    // create connection object that will transfer the dsata from server
    ConnectionToDatabase *connection = [[ConnectionToDatabase alloc]initWithRequest:req];
    
    //when connection completes, run the block
    [connection setCompletionBlock:block];
    
    //create empty container of course
    //User *user = [[User alloc]init];
    
    //conforms to protocol
    //[connection setJSONRootObject:user];
    
    [connection start];
}
-(void)postData:(NSData *)data block:(void (^)(NSDictionary *dictionary, NSError *err))block
{
    NSString *urlWithId = [NSString stringWithFormat:@"%@", BASE_URL];
    
    NSMutableString *urlAsString = [[NSMutableString alloc] initWithString:urlWithId];
    
    NSURL *url = [NSURL URLWithString:urlAsString];

     NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    // set method
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:data];
    //set headers
    NSString *contentType = [NSString stringWithFormat:@"application/json"];
    [urlRequest addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // create connection object that will transfer the dsata from server
    ConnectionToDatabase *connection = [[ConnectionToDatabase alloc]initWithRequest:urlRequest];
    
    //when connection completes, run the block
    [connection setCompletionBlock:block];
    [connection start];

}
-(NSData*)createJsonFromDictionary:(NSDictionary*)dictionary
{
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
    return jsonData;
}



@end
