//
//  AdminCourseDetailViewController.h
//  Schedule
//
//  Created by susan on 10/18/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Course.h"

@interface AdminCourseDetailViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *courseNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *courseIdLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic , strong)Course *selectedCourse;

@end
