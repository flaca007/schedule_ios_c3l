//
//  StudentWeeklyScheduleTableViewController.m
//  Schedule
//
//  Created by susan on 10/23/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import "StudentWeeklyScheduleTableViewController.h"
#import "UserStore.h"
#import "CourseEvent.h"
#import "StudentWeeklyReadingInstructionsTableViewController.h"
#import "StudentEventDetailsStaticTableViewController.h"
#import "Utilities.h"
@interface StudentWeeklyScheduleTableViewController ()

@end

@implementation StudentWeeklyScheduleTableViewController
{
    Utilities *utilities;
    NSDateFormatter *formatter;

}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    utilities = [[Utilities alloc]init];
//    formatter = [utilities dateFormatterWithDate];
    self.navigationController.navigationBarHidden = NO;
    
    self.navigationItem.title = [NSString stringWithFormat:@"Week %d", [self selectedWeek]];

    [self setWeekSchedule:[[[UserStore sharedStore]activeUser]weeklyEvents:[self selectedWeek]]];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    switch (section) {
        case 0:
            return [[[self weekSchedule]objectAtIndex:0]count];
            break;
        case 1:
            return [[[self weekSchedule]objectAtIndex:1]count];
            break;
        case 2:
            return [[[self weekSchedule]objectAtIndex:2]count];
            break;
        case 3:
            return [[[self weekSchedule]objectAtIndex:3]count];
            break;
        case 4:
            return [[[self weekSchedule]objectAtIndex:4]count];
            break;
            
        default:
            break;
    }
    //return [[[[UserStore sharedStore]activeUser]weeklySchema:[self selectedWeek]]count];
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return @"Monday";
            break;
        case 1:
            return @"Tuesday";
            break;
        case 2:
            return @"Wednesday";
            break;
        case 3:
            return @"Thursday";
            break;
        case 4:
            return @"Friday";
            break;
            
        default:
            break;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"weeklyScheduleCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
   // NSArray *eventArray = [NSArray arrayWithArray:[[[UserStore sharedStore]activeUser]weeklySchema:[self selectedWeek]]];

    // Configure the cell...
   
    CourseEvent *event;// = [eventArray objectAtIndex:[indexPath row]];
//    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
//    [formatter setDateFormat:@"HH:mm"];
    utilities = [[Utilities alloc]init];
    NSDateFormatter *formatter = [utilities dateFormatterWithTime];
    
    switch (indexPath.section) {
        case 0:
            event = [[[self weekSchedule] objectAtIndex:0] objectAtIndex:[indexPath row]];
            break;
        case 1:
            event = [[[self weekSchedule] objectAtIndex:1] objectAtIndex:[indexPath row]];
            break;
        case 2:
            event = [[[self weekSchedule] objectAtIndex:2] objectAtIndex:[indexPath row]];
            break;
        case 3:
            event = [[[self weekSchedule] objectAtIndex:3] objectAtIndex:[indexPath row]];
            break;
        case 4:
            event = [[[self weekSchedule] objectAtIndex:4] objectAtIndex:[indexPath row]];
            break;
            
        default:
            break;
    }
    [[cell textLabel]setText:[NSString stringWithFormat:@"%@",[event eventDescription]]];
    [[cell detailTextLabel]setText:[NSString stringWithFormat:@"%@-%@",[formatter stringFromDate:[event eventStartDate]], [formatter stringFromDate:[event eventEndDate]]]];
    

    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}
-(void)changeWeek:(int)weekToChange
{
    self.selectedWeek = self.selectedWeek + weekToChange;
    [self setWeekSchedule:[[[UserStore sharedStore]activeUser]weeklyEvents:[self selectedWeek]]];
    self.navigationItem.title = [NSString stringWithFormat:@"Week %d", [self selectedWeek]];


    [[self tableView]reloadData];
}
- (IBAction)nextWeek:(id)sender
{
    [self changeWeek:1];
}

- (IBAction)preWeek:(id)sender
{
    [self changeWeek:-1];
}
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([segue.identifier isEqualToString:@"toWeeklyReadingInstructions"])
    {

        StudentWeeklyReadingInstructionsTableViewController *studentWeeklyReadingInstructionsTableViewController = [segue destinationViewController];
        [studentWeeklyReadingInstructionsTableViewController setEventArray:[[[UserStore sharedStore]activeUser]weeklyEvents:[self selectedWeek]]];
        
    }
    if ([segue.identifier isEqualToString:@"toEventDetails"])
    {
        NSIndexPath *indexPath=[self.tableView indexPathForSelectedRow];
        StudentEventDetailsStaticTableViewController *studentEventReadingInstructionStaticTableViewController = [segue destinationViewController];
        switch (indexPath.section) {
            case 0:
                [studentEventReadingInstructionStaticTableViewController setSelectedEvent:[[[self weekSchedule]objectAtIndex:0] objectAtIndex:[indexPath row]]];
                break;
            case 1:
                [studentEventReadingInstructionStaticTableViewController setSelectedEvent:[[[self weekSchedule]objectAtIndex:1] objectAtIndex:[indexPath row]]];
                break;
            case 2:
                [studentEventReadingInstructionStaticTableViewController setSelectedEvent:[[[self weekSchedule]objectAtIndex:2] objectAtIndex:[indexPath row]]];
                break;
            case 3:
                [studentEventReadingInstructionStaticTableViewController setSelectedEvent:[[[self weekSchedule]objectAtIndex:3] objectAtIndex:[indexPath row]]];
                break;
            case 4:
                [studentEventReadingInstructionStaticTableViewController setSelectedEvent:[[[self weekSchedule]objectAtIndex:4] objectAtIndex:[indexPath row]]];
                break;
                
            default:
                break;
        }
        

        
        NSLog(@"studenteventreadingins: %@", [studentEventReadingInstructionStaticTableViewController selectedEvent]);
    }

    
}

@end
