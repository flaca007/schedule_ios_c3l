//
//  LoginViewController.h
//  Schedule_c3l
//
//  Created by susan on 10/31/12.
//  Copyright (c) 2012 Susan Sarabia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *userName;
@property (weak, nonatomic) IBOutlet UITextField *password;

- (IBAction)login:(id)sender;
@end
