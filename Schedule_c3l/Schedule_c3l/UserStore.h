//
//  UserStorage.h
//  Schedule
//
//  Created by susan on 10/10/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "Store.h"
@interface UserStore : Store
{
    NSMutableArray *allUsers;
    User *activeUser;
    NSMutableDictionary *userData;
}

+(UserStore *)sharedStore;

-(void)createUser:(NSDictionary *)userDictionary;

-(void)setActiveUser:(User *)user;
-(NSArray *)allUsers;
-(User *)activeUser;
-(NSDictionary *)userData;
-(void)setUserdataDictionary:(NSMutableDictionary *)dictionary;


-(void)logInUser:(NSString *)userId callback:(void (^)(void))callback;

-(void)logInUser:(NSString *)username password:(NSString *)password callback:(void (^)(void))callback;



-(void)getActiveUserData:(void (^)(NSError *error))callback;


@end
