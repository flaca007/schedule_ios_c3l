//
//  AdminStore.m
//  Schedule
//
//  Created by susan on 10/17/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import "AdminStore.h"
#import "CourseStore.h"
#import "UserStore.h"
#import "Course.h"
#import "Message.h"
@implementation AdminStore
+(AdminStore *)sharedStore
{
    static AdminStore *sharedStore;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        
        sharedStore = [[self alloc]init];
        
    });
    return sharedStore;
}
-(void)sendMessage:(Message *)message callback:(void (^)(NSError *error))callback
{
    NSData *data = [[NSData alloc]init];
    //if(!isNew){
    data = [self createJsonFromDictionary:[message saveMessageAsDictionary]];
        
    //}
    [self postData:data block:^(NSDictionary *dictionary, NSError *err) {
        // code
        if(!err){
            NSLog(@"dictionary from post data: %@", dictionary);
            [message setDb_rev:[dictionary valueForKey:@"rev"]];
            [message setDb_id:[dictionary valueForKey:@"id"]];

            NSLog(@"newly created  message: %@", message);
            
        }
        callback(err);
    }];
}
-(void)getAdminData:(void (^)(NSError *error))callback
{
    NSString *urlAllAdminData = [NSString stringWithFormat:@"_design/views/_view/getUsersAndCourses"];
    [self fetchData:urlAllAdminData block:^(NSDictionary *dictionary, NSError *err) {
        if(!err){
            NSMutableArray* tempAllCoursesArray = [[NSMutableArray alloc]init];
            NSArray *adminArray = [dictionary  valueForKey:@"rows"];
            for(NSDictionary *valueDictionary in [adminArray valueForKey:@"value"]){
                
                if([[valueDictionary valueForKey:@"type"]isEqualToString:@"course"]){
                    
                    [tempAllCoursesArray addObject:valueDictionary];
                }
                if([[valueDictionary valueForKey:@"type"]isEqualToString:@"user"]){
                    
                    if([[valueDictionary valueForKey:@"role"]isEqualToString:@"Student"]){
                        
                        [[UserStore sharedStore]createUser:valueDictionary];
                    }
                }
            }
            for (id allDictionary in tempAllCoursesArray ){
                
                Course *tempCourse;
                tempCourse = [[CourseStore sharedStore]createCourseWithEvents:allDictionary];

                NSArray *courseStudents = [NSArray arrayWithArray:[allDictionary valueForKey:@"courseStudents"]];
                for(NSString *studentId in courseStudents){
                    
                    NSMutableArray *tempAllUsersArray = [NSMutableArray arrayWithArray:[[UserStore sharedStore]allUsers]];
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"db_id == %@", studentId];
                    NSArray *toBeReclaimed = [tempAllUsersArray filteredArrayUsingPredicate:predicate];
                    if([toBeReclaimed count] > 0) {
                        
                        [tempCourse addStudentToCourse:[toBeReclaimed objectAtIndex:0]];
                        [[toBeReclaimed objectAtIndex:0]addCourseToUser:tempCourse];
                    }
                }
            }
        }
        callback(err);
    }];

}
@end
