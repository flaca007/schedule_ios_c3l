//
//  StudentHomePageTableViewController.h
//  Schedule
//
//  Created by susan on 10/24/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StudentHomePageTableViewController : UITableViewController
@property(nonatomic)NSArray *dayEvents;

@end
