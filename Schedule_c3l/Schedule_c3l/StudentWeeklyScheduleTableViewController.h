//
//  StudentWeeklyScheduleTableViewController.h
//  Schedule
//
//  Created by susan on 10/23/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StudentWeeklyScheduleTableViewController : UITableViewController
@property(nonatomic)int selectedWeek;
@property(nonatomic)NSArray *weekSchedule;

- (IBAction)nextWeek:(id)sender;
- (IBAction)preWeek:(id)sender;
-(void)changeWeek:(int)weekToChange;

@end
