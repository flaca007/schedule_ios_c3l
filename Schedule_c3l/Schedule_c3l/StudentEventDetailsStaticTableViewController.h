//
//  StudentEventReadingInstructionStaticTableViewController.h
//  Schedule
//
//  Created by susan on 10/23/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CourseEvent.h"
@interface StudentEventDetailsStaticTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UILabel *startDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *enddateLabel;
@property (weak, nonatomic) IBOutlet UILabel *classroomLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *readingInstructionLabel;

@property(nonatomic, strong)CourseEvent *selectedEvent;
@end
