//
//  Utilities.h
//  Schedule
//
//  Created by susan on 10/25/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utilities : NSObject
-(NSDate *)dateWithRightTimeZone;
-(NSDateFormatter *)dateFormatter;
-(NSDateFormatter *)dateFormatterWithTime;
-(NSDateFormatter *)dateFormatterWithDateTime;
-(NSDateFormatter *)dateFormatterWithDate;


@end
