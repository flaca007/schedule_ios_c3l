//
//  EventeditViewController.m
//  Schedule_c3l
//
//  Created by susan on 10/31/12.
//  Copyright (c) 2012 Susan Sarabia. All rights reserved.
//

#import "EventeditViewController.h"
#import "Utilities.h"
#import "CourseStore.h"
#import "AdminDatePickerViewController.h"
@interface EventeditViewController ()

@end

@implementation EventeditViewController
{
    UITextField *activeField;
    Utilities *utilities;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.isNew = NO;
    if(![self selectedEvent]){
        self.selectedEvent = [[CourseEvent alloc]init];
        self.isNew = YES;
        
    }
    NSLog(@"selected event: %@", [self selectedEvent]);
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    [[self eventClassroom]setText:[[self selectedEvent]classRoom]];
    [[self eventAltTeacher]setText:[[self selectedEvent]alternativeTeacher]];
    
    
    [[self eventReadingInstructions]setText:[[self selectedEvent]eventReadingInstructions]];
    [[self eventDescription]setText:[[self selectedEvent]eventDescription]];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    utilities = [[Utilities alloc]init];
    NSDateFormatter *formatter = [utilities dateFormatterWithDateTime];
    
    [[self startDate]setTitle:[NSString stringWithFormat:@"Start Date - %@",[formatter stringFromDate:[[self selectedEvent]eventStartDate]]] forState:UIControlStateNormal];
    [[self endDate]setTitle:[NSString stringWithFormat:@"End Date - %@",[formatter stringFromDate:[[self selectedEvent]eventEndDate]]] forState:UIControlStateNormal];
    
    
    //    [[cell detailTextLabel]setText:[NSString stringWithFormat:@"%@-%@",[formatter stringFromDate:[event eventStartDate]], [formatterTime stringFromDate:[event eventEndDate]]]];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)save:(id)sender
{
    [self setTheOldEvent:[self selectedEvent]];
    
    if([self isNew]){
        [[self selectedCourse]addCourseEvent:[self selectedEvent]];
    }
    
    [[self selectedEvent]setClassRoom:self.eventClassroom.text];
    [[self selectedEvent]setAlternativeTeacher:self.eventAltTeacher.text];
    [[self selectedEvent]setEventDescription:self.eventDescription.text];
    [[self selectedEvent]setEventReadingInstructions:self.eventReadingInstructions.text];
    
    
    NSLog(@"selected course: %@", [self selectedCourse]);
    [[CourseStore sharedStore]saveCourse:[self selectedCourse] isNew:NO block:^(NSError *err) {
        //code
        NSLog(@"back in admin event edit view controller: %@", err);
        if(!err){
            [[self navigationController]popViewControllerAnimated:YES];
        }else{
            self.selectedEvent = self.theOldEvent;
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"ALERT" message:@"It did not go well" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
}

- (IBAction)editStartdate:(id)sender
{
    [self setIsStartDateSelected:YES];
    [self performSegueWithIdentifier:@"toDatePickerViewController" sender:self];
}

- (IBAction)editEndDate:(id)sender
{
    [self setIsStartDateSelected:NO];
    [self performSegueWithIdentifier:@"toDatePickerViewController" sender:self];
}
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"toDatePickerViewController"])
    {
        
        AdminDatePickerViewController *adminDatePickerController = [segue destinationViewController];
        [adminDatePickerController setSelectedCourse:[self selectedCourse]];
        [adminDatePickerController setSelectedEvent:[self selectedEvent]];
        [adminDatePickerController setIsStartDateSelected:[self isStartDateSelected]];
        
    }
    
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-kbSize.height);
        [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}
@end
