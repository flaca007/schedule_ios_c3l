//
//  AdminDatePickerViewController.m
//  Schedule
//
//  Created by susan on 10/18/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import "AdminDatePickerViewController.h"
#import "CourseEvent.h"
#import "CourseStore.h"
#import "Utilities.h"
@interface AdminDatePickerViewController ()

@end

@implementation AdminDatePickerViewController
{
    Utilities *utilities;
    NSDateFormatter *formatter;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    utilities = [[Utilities alloc]init];
    formatter = [utilities dateFormatterWithDateTime];

    self.tempStartDate = [[self selectedEvent]eventStartDate];
    self.tempEndDate = [[self selectedEvent]eventEndDate];

    [[self datePicker]setDatePickerMode:UIDatePickerModeDateAndTime];
    [[self datePicker]setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT" ]];


   
    [self datePickerToggleDate];
    [[self datePicker]addTarget:self action:@selector(dateChange) forControlEvents:UIControlEventValueChanged];


	// Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{

    [super viewWillAppear:animated];

    [[self startdateBtn]setTitle:[NSString stringWithFormat:@"Start - %@", [formatter stringFromDate:[self tempStartDate]]] forState:UIControlStateNormal];
    [[self endDateBtn]setTitle:[NSString stringWithFormat:@"End - %@", [formatter stringFromDate:[self tempEndDate]]]  forState:UIControlStateNormal];
    
}
-(void)dateChange
{

    if([self isStartDateSelected]){
        self.tempStartDate =  self.datePicker.date;
        [[self startdateBtn]setTitle:[NSString stringWithFormat:@"Start - %@", [formatter stringFromDate:[self tempStartDate]]] forState:UIControlStateNormal];

    }else{
        self.tempEndDate = self.datePicker.date;
        [[self endDateBtn]setTitle:[NSString stringWithFormat:@"End - %@", [formatter stringFromDate:[self tempEndDate]]]  forState:UIControlStateNormal];

    }
}
-(void)datePickerToggleDate
{

    if([self isStartDateSelected]){
        [[self startdateBtn]setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
        [[self datePicker]setDate:[self tempStartDate] animated:YES];
        [[self endDateBtn]setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];

        // fix date picker set value , gives wrong date

    }else{
        [[self endDateBtn]setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
        [[self startdateBtn]setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];

        [[self datePicker]setDate:[self tempEndDate] animated:YES];

    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)editStartDate:(id)sender
{
    [self setIsStartDateSelected:YES];
    [self datePickerToggleDate];

}

- (IBAction)editEndDate:(id)sender
{
    [self setIsStartDateSelected:NO];    
    [self datePickerToggleDate];
}

- (IBAction)saveDate:(id)sender
{
    [self setTheOldEvent:[self selectedEvent]];
    
    [[self selectedEvent]setEventStartDate:[self tempStartDate]];
    [[self selectedEvent]setEventEndDate:[self tempEndDate]];
    
    NSLog(@"selected course: %@", [self selectedCourse]);
    [[CourseStore sharedStore]saveCourse:[self selectedCourse] isNew:NO block:^(NSError *err) {
        //code
        NSLog(@"back in admin date picker view controller: %@", err);
        if(!err){
            [[self navigationController]popViewControllerAnimated:YES];
        }else{
            self.selectedEvent = self.theOldEvent;
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"ALERT" message:@"It did not go well" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}
@end
