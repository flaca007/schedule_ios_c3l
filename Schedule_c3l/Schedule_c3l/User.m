
#import "User.h"
#import "Course.h"
#import "CourseEvent.h"

#import "Message.h"
#import "UserStore.h"
#import "Utilities.h"
//extern NSString *const ATRoleStudent = @"Student";
//extern NSString *const ATRoleTeacher = @"Teacher";
//extern NSString *const ATRoleAdmin = @"Admin";
//
//extern NSString *const ATUserStatusActive = @"Active";
//extern NSString *const ATUserStatusInactive = @"Inactive";

@implementation User

{
    NSArray *userCourses;
    NSArray *userMessages;

}


+(id) userFromDictionary:(NSDictionary*) dictionary {
    
    return [self userWithUserEmail:[dictionary valueForKey:@"email"]
                          username:[dictionary valueForKey:@"name"]
                          lastName:[dictionary valueForKey:@"lastName"]
                              role:[dictionary valueForKey:@"role"]
                             db_id: [dictionary valueForKey:@"_id"]
                            db_rev: [dictionary valueForKey:@"_rev"]
                            status: [dictionary valueForKey:@"status"]
                            type:[dictionary valueForKey:@"type"]];
}

+(id)userFromDictionaryWithCourses:(NSDictionary*)dictionaryWithCourses { // there are messages here too
    
    User *newUser = [self userFromDictionary:dictionaryWithCourses];
    // errors
//    //UserServices *userServices = [[UserServices alloc]init];
//    //Services *service = [[Services alloc]init];
//    
//    //NSLog(@"[dictionaryWithCourses: %@", [dictionaryWithCourses valueForKey:@"studentCourses"]);
//    
 //   for(NSString *courseId in [dictionaryWithCourses valueForKey:@"studentCourses"]){
//
        
        //Course *newCourse = [Course courseFromDictionaryWithEvents:[service getUniqeDoc:courseId]];
//
//        [newUser addCourseToUser:newCourse];
//        //NSLog(@"courseid: %@",courseId);
 //   }
//    //NSLog(@"new user: %@", newUser);
//   NSArray *arrayWithDictionaryWithMessages = [userServices dictionaryFromMessageDbJson:[newUser db_id]]; 
//    for(NSDictionary *message in arrayWithDictionaryWithMessages){
//        //NSLog(@"the messages: %@",[message valueForKey:@"value"]);
//        Message *tempMessage = [Message messageFromDictionary:[message valueForKey:@"value"]];
//        [newUser addMessageToUser:tempMessage];
//    }
//    //NSLog(@"newUser: %@", newUser);
//    //[newUser getMessages];
    return newUser;
}



+(id)userWithUserEmail:(NSString*)userEmail username:(NSString*)userName lastName:(NSString*)lastName role:(NSString*)role db_id:(NSString*)db_id db_rev:(NSString*)db_rev status:(NSString*)status type:(NSString *)type{
    return [[self alloc] initWithUserEmail:userEmail username:userName lastName:lastName role:role db_id:db_id db_rev: db_rev status:status type:type];
}

-(id) init {
    return [self initWithUserEmail:@"no-user-email" username:@"no-username" lastName:@"no-userlastName" role:@"no-user-role" db_id: @"no _id" db_rev:@"no _rev" status:@"no-status" type:@"user"];
}

-(id)initWithUserEmail:(NSString*)userEmail username:(NSString*)userName lastName:(NSString*)lastname role:(NSString*)role db_id:(NSString*)db_id db_rev:(NSString*)db_rev status:(NSString*)status type:(NSString *)type{
    if(self = [super init]) {
        _userName = [userName copy];
        _lastName = [lastname copy];
        _userEmail = [userEmail copy];
        _userRole = [role copy];
        _db_id = [db_id copy];
        _db_rev = [db_rev copy];
        _status = [status copy];
        userCourses = [NSArray array];
        userMessages = [NSArray array];
        _type = type;
        
    }
    return self;
}
-(NSString*)simpleDescription {
    return [NSString stringWithFormat:@"Namn: %@ Efternamn: %@ role: %@\n", self.userName, self.lastName, self.userRole];
    
}
-(NSString*) description {
    [self getCoursesIds];
    return [NSString stringWithFormat:@" %@, %@, %@, %@, %@, %@, %@, COURSES: %@, messages: %@", self.userName, self.lastName, self.userEmail, self.userRole, self.db_id, self.db_rev, self.status, userCourses, userMessages];
}
// create new dictionary with new user
-(NSDictionary*)saveUserAsDictionary {
    NSDictionary *dictionaryWithUser = [NSDictionary dictionaryWithObjectsAndKeys:self.userName, @"name",self.lastName, @"lastName",self.userEmail, @"email", self.userRole, @"role", self.status, @"status", nil];
    return dictionaryWithUser;
}
// create new dictionary with update user
-(NSDictionary*)updateUserAsDictionary {
    
    NSArray *courseListinStudent = [NSArray arrayWithArray:[self getCoursesIds]];

    NSDictionary *dictionaryWithUser = [NSDictionary dictionaryWithObjectsAndKeys:self.userName, @"name",self.lastName, @"lastName",self.userEmail, @"email", self.userRole, @"role", self.db_id, @"_id", self.db_rev, @"_rev", self.status, @"status",courseListinStudent, @"studentCourses",  nil];
    
    //NSLog(@"dictionaryWithUser update _id, _rev: %@",dictionaryWithUser);
    return dictionaryWithUser;
}
//errors
//-(void)updateUser {
//    service = [[Services alloc]init];
//    NSMutableDictionary *resultDictionary = [NSMutableDictionary dictionary];
//    [resultDictionary setDictionary:[service saveToDb:[self updateUserAsDictionary]]];
//    [self setDb_rev:[resultDictionary valueForKey:@"rev"]];
//}
//-(void)readFromJSONDictionary:(NSDictionary *)d
//{
//    NSLog(@"protocol implementation in course: %@", d);
//    [Course courseFromDictionaryWithEvents:d];//this never comesback to the delegate
//}
-(void) addCourseToUser:(id) course
{
    NSMutableArray* newArray = [NSMutableArray arrayWithArray:userCourses];
    
    [newArray addObject:course];
          
    userCourses = newArray;
}
-(NSArray*)getCoursesIds
{
    NSMutableArray *courseIdList = [NSMutableArray array];
    for(Course *course in userCourses)
    {
        [courseIdList addObject:course.db_courseId];
    }
    return courseIdList;
    
}
-(NSArray*) allCourses
{
    NSArray *courses = [NSArray arrayWithArray:userCourses];
    return courses;
}
-(void) addMessageToUser:(Message*) message
{
    NSMutableArray* newArray = [NSMutableArray arrayWithArray:userMessages];
    
    [newArray addObject:message];
    
    userMessages = newArray;

}

-(NSArray*) allCourseEvents
{
    NSMutableArray *totalEvents = [NSMutableArray array];
    for(Course* course in userCourses)
    {
        for(CourseEvent *event in [course allEvents])
        {
            [totalEvents addObject:event];
        }
    }
    
    return totalEvents;
}
-(NSArray*)getMessages {
    
    NSMutableArray* newArray = [NSMutableArray arrayWithArray:userMessages];
    NSSortDescriptor *sortDesc = [NSSortDescriptor sortDescriptorWithKey:@"sentDate" ascending:FALSE];
    NSArray *sortDecArray = [NSArray arrayWithObject:sortDesc];
    userMessages = [newArray sortedArrayUsingDescriptors:sortDecArray];
    return userMessages;    
}
-(NSArray*) dailySchema:(NSDate*) dateToShow
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
//    NSDate *startDate = [dateFormatter dateFromString:[dictionaryWithEvent valueForKey:@"eventStartDate"]];
//    NSDate *endDate = [dateFormatter dateFromString:[dictionaryWithEvent valueForKey:@"eventEndDate"]];
    
    NSMutableArray *dailyEvents = [NSMutableArray array];
    
//    NSString *dateToShowString = [dateToShow descriptionWithCalendarFormat:@"%Y-%m-%d" timeZone:nil locale:
//                                  [[NSUserDefaults standardUserDefaults] dictionaryRepresentation]];
    
    for(Course* course in userCourses)
    {
        for(CourseEvent *event in [course allEvents])
        {
            if([[dateFormatter stringFromDate:dateToShow] isEqualToString:[dateFormatter stringFromDate:[event eventStartDate]]])
            {
                NSLog(@"dates  are good to go");
                [dailyEvents addObject:event];
            }                
        }
    }
    NSLog(@"daily events: %@", dailyEvents);
    return dailyEvents;
}

-(NSArray *)nextOrOngoingEvent
{
// fix timezone when comparing the dates
    Utilities *utilities = [[Utilities alloc]init];
    NSDate *now = [utilities dateWithRightTimeZone];
    
    NSMutableArray *temDailySchema = [NSMutableArray arrayWithArray:[self dailySchema:now]];
    //[self dailySchema:[self dateWithRightTimeZone]];
    
    NSSortDescriptor *sortByDate = [NSSortDescriptor sortDescriptorWithKey:@"eventStartDate" ascending:TRUE];
    NSArray *sortDecArray = [NSArray arrayWithObjects:sortByDate, nil];
    //for(NSMutableArray *day in weeklyEvents){
    [temDailySchema sortedArrayUsingDescriptors:sortDecArray];
    //}
    NSMutableArray *tempEvents = [NSMutableArray array];
    if(temDailySchema.count == 0){
        return nil;
    }else{
        //do work
       
        for(int i = 0; i < temDailySchema.count; i++){
            CourseEvent *event = [temDailySchema objectAtIndex:i];
//            
//            NSDate *tempDate = [[event eventStartDate]copy];
//            [utilities dateWithRightTimeZone:tempDate];
            
            NSComparisonResult result = [now compare:event.eventStartDate];
            if(NSOrderedAscending == result){
                [tempEvents addObject:event];
                if(temDailySchema.count > i + 1 ){

                    event = [temDailySchema objectAtIndex:i + 1];
                     [tempEvents addObject:event];
                    

                    
                }
                return tempEvents;
            }
            if(NSOrderedDescending == result){
                NSComparisonResult resultEndDate = [now compare:event.eventEndDate];
                if(NSOrderedAscending == resultEndDate){
                    [tempEvents addObject:event];
                    if(temDailySchema.count > i + 1){
                        event = [temDailySchema objectAtIndex:i + 1];
                        [tempEvents addObject:event];
                        
                    }
                    return tempEvents;
                }
                
            }
        }

    }
    return tempEvents;
}
-(NSArray*) weeklySchema:(NSInteger) weekNum
{

    // create a calendar
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    if(weekNum == 0 || weekNum > 52)
    {   
        NSDate *today = [NSDate date];
        weekNum = [[calendar components: NSWeekCalendarUnit fromDate:today] week];
    }
    
    //NSLog(@"Vecka: %ld", weekNum);
    NSMutableArray *weeklyEvents = [NSMutableArray array];
    for(Course* course in userCourses)
    {
        if(weekNum >= [course startWeek] && weekNum <= [course endWeek])
        {   
            //NSLog(@"%@", course);
            for(CourseEvent *event in [course allEvents])
            {   
                if(weekNum == [[calendar components: NSWeekCalendarUnit fromDate:[event eventStartDate]] week])
                {
                    // NSLog(@"%@", [event eventDate]);
                    [weeklyEvents addObject:event];
                }
            }
        }
    }
    NSSortDescriptor *sortByDate = [NSSortDescriptor sortDescriptorWithKey:@"eventStartDate" ascending:TRUE];
    NSArray *sortDecArray = [NSArray arrayWithObjects:sortByDate, nil];
    return [weeklyEvents sortedArrayUsingDescriptors:sortDecArray];
}
-(NSArray *)weeklyEvents:(NSInteger)weekNum
{
    NSMutableArray *monday = [NSMutableArray array];
    NSMutableArray *tuesday = [NSMutableArray array];
    NSMutableArray *wednesday = [NSMutableArray array];
    NSMutableArray *thursday = [NSMutableArray array];
    NSMutableArray *friday = [NSMutableArray array];
    

    // create a calendar
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    if(weekNum == 0 || weekNum > 52)
    {
        NSDate *today = [NSDate date];
        weekNum = [[calendar components: NSWeekCalendarUnit fromDate:today] week];
    }
    
    //NSLog(@"Vecka: %ld", weekNum);
    NSMutableArray *weeklyEvents = [NSMutableArray arrayWithObjects:monday,tuesday, wednesday, thursday, friday, nil];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"c"];
    
    for(Course* course in userCourses)
    {
        if(weekNum >= [course startWeek] && weekNum <= [course endWeek])
        {
            //NSLog(@"%@", course);
            for(CourseEvent *event in [course allEvents])
            {
                if(weekNum == [[calendar components: NSWeekCalendarUnit fromDate:[event eventStartDate]] week])
                {
                   NSString *dayNumber = [formatter stringFromDate:[event eventStartDate]];
                
                    int dayValue = [dayNumber intValue];
                    switch (dayValue) {
                        case 1:
                            [monday addObject:event];
                            break;
                        case 2:
                            [tuesday addObject:event];
                            break;
                        case 3:
                            [wednesday addObject:event];
                            break;
                        case 4:
                            [thursday addObject:event];
                            break;
                        case 5:
                            [friday addObject:event];
                            break;
                        default:
                            break;
                    }
                    // NSLog(@"%@", [event eventDate]);
                   // [weeklyEvents addObject:event];
                }
            }
        }
    }
    NSSortDescriptor *sortByDate = [NSSortDescriptor sortDescriptorWithKey:@"eventStartDate" ascending:TRUE];
    NSArray *sortDecArray = [NSArray arrayWithObjects:sortByDate, nil];
    for(NSMutableArray *day in weeklyEvents){
        [day sortedArrayUsingDescriptors:sortDecArray];
    }

    return weeklyEvents;
}
//
//-(void) dailyInstructions:(NSDate*) dateToShow
//{
//   
//    NSLog(@"Daily instruct");
//    NSArray *events = [NSArray arrayWithArray:[self dailySchema:dateToShow]];
//    if ([events count] >= 1)
//    {
//        for(CourseEvent *event in events)
//        {
//            NSLog(@"%@ %@\n",[event eventStartDate], [event eventReadingInstructions]);
//        }
//    }
//    else
//    {
//        //NSLog(@"%@\n", [[events objectAtIndex:0] eventReadingInstructions]);
//        NSLog(@"There is nothing save on db");
//    }
//    NSLog(@"\n");
//    
//}
//-(void) weeklyInstructions:(NSInteger) weekNum
//{
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
//    
//    NSLog(@"Weeky instruct");
//    NSArray *events = [NSArray arrayWithArray:[self weeklySchema:weekNum]];
//
//    NSString *prevDay, *currentDay;
////errors
//    // *** Code for demo for displaying weekly intructions in the console
//    for(CourseEvent *event in events)
//    {
//        currentDay =  [[event eventStartDate] descriptionWithCalendarFormat:@"%A" timeZone:nil locale:[[NSUserDefaults standardUserDefaults] dictionaryRepresentation]];
//        
//        if(![prevDay isEqualToString:currentDay])
//         {  
//            NSLog(@"%@", currentDay);
//             prevDay = currentDay;
//         }
//        NSLog(@"%@\n", [event eventReadingInstructions]);
//    }
//     NSLog(@"\n");
//}
@end
