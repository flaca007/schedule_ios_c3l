//
//  StudentHomePageTableViewController.m
//  Schedule
//
//  Created by susan on 10/24/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import "StudentHomePageTableViewController.h"
#import "UserStore.h"
#import "User.h"
#import "CourseEvent.h"
#import "StudentDailyScheduleTableViewController.h"
#import "StudentWeeklyScheduleTableViewController.h"
#import "Utilities.h"
#import "Message.h"
@interface StudentHomePageTableViewController ()

@end

@implementation StudentHomePageTableViewController
{

    Utilities *utilities;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.toolbarHidden = false;
    self.navigationController.navigationBarHidden = YES;

    
    [[UserStore sharedStore] getActiveUserData:^(NSError *error) {
        //put activity indicator here
        [self setDayEvents:[[[UserStore sharedStore]activeUser]nextOrOngoingEvent]];

        [[self tableView]reloadData];
    }];

    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if([[self dayEvents]count] == 0){
        return 1;
    }else{
        return [[self dayEvents]count];

    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return @"Next or Now";
            break;
        case 1:
            return @"After";
            break;
            
        default:
            break;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"todayCourseCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    CourseEvent *event;
    utilities = [[Utilities alloc]init];
    NSDateFormatter *formatter = [utilities dateFormatterWithTime];

    
    switch (indexPath.section) {
        case 0:
            event = [[self dayEvents] objectAtIndex:0];
            [[cell textLabel]setText:[NSString stringWithFormat:@"%@",[event eventDescription]]];
            
            [[cell detailTextLabel]setText:[NSString stringWithFormat:@"%@-%@ (Sal: %@)",[formatter stringFromDate:[event eventStartDate]], [formatter stringFromDate:[event eventEndDate]],[event classRoom]]];
            NSLog(@"event: %@", event);
            break;
        case 1:
            NSLog(@"case 1");

            event = [[self dayEvents] objectAtIndex:1];
            [[cell textLabel]setText:[NSString stringWithFormat:@"%@",[event eventDescription]]];
            
            [[cell detailTextLabel]setText:[NSString stringWithFormat:@"%@-%@ (Sal: %@)",[formatter stringFromDate:[event eventStartDate]], [formatter stringFromDate:[event eventEndDate]],[event classRoom]]];
            break;
            
        default:
            break;
    }


    
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSDate *today = [[NSDate alloc]init];
    
    if ([segue.identifier isEqualToString:@"toDailySchedule"])
    {
        //        NSIndexPath *indexPath=[self.tableView indexPathForSelectedRow];
        //        //NSLog(@"indexPath: %@", indexPath);
        //
        //        //NSLog(@"XXXXX :%@",[[[CourseStore sharedStore] allCourses] objectAtIndex:[indexPath row]]);
        StudentDailyScheduleTableViewController *studentDailyScheduleTableViewController = [segue destinationViewController];
        [studentDailyScheduleTableViewController setSelectedDate:today];
        
    }
    if([segue.identifier isEqualToString:@"toWeeklySchedule"])
    {
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        
        StudentWeeklyScheduleTableViewController *studentWeeklyScheduleTableViewController = [segue destinationViewController];
        [studentWeeklyScheduleTableViewController setSelectedWeek:[[calendar components: NSWeekCalendarUnit fromDate:today] week]];
    }
    
}
@end
