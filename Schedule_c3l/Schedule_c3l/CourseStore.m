//
//  CourseStorage.m
//  Schedule
//
//  Created by susan on 10/11/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import "CourseStore.h"
#import "Course.h"
#import "ConnectionToDatabase.h"

@implementation CourseStore

+(CourseStore *)sharedStore
{
    static CourseStore *sharedStore;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        
        sharedStore = [[self alloc]init];
        sharedStore->allCourses = [NSMutableArray array];
        
    });
    return sharedStore;
}
-(id)init
{
    self = [super init];
    if(self){
        allCourses = [[NSMutableArray alloc]init];
    }
    return self;
}
-(NSArray *)allCourses
{
    return allCourses;
}
-(Course *)createCourseWithEvents:(NSDictionary *)courseDictionary
{
    Course *course = [Course courseFromDictionaryWithEvents:courseDictionary];
    [allCourses addObject:course];
    //NSLog(@"course array from course store: %@", allCourses);

    return course;
}
-(void)saveCourse:(Course *)course isNew:(BOOL)isNew block:(void (^)(NSError *err))block
{
    NSData *data = [[NSData alloc]init];
    if(!isNew){
        data = [self createJsonFromDictionary:[course updateCourseAsDictionary]];
        
    }
    [self postData:data block:^(NSDictionary *dictionary, NSError *err) {
       // code
        if(!err){
            NSLog(@"dictionary from post data: %@", dictionary);
            [course setDb_courseRev:[dictionary valueForKey:@"rev"]];
            NSLog(@"newly updated course: %@", course);

        }
        block(err);
    }];

}
@end
