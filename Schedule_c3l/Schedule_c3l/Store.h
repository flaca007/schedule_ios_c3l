//
//  Store.h
//  Schedule
//
//  Created by susan on 10/15/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import <Foundation/Foundation.h>

#define BASE_URL @"http://127.0.0.1:5984/schedule/"

//#define BASE_URL @"http://stockholmsguiden2.iriscouch.com/schedule/"
//#define NEW_URL @"http://stockholmsguiden2.iriscouch.com/schedule/_design/views/_view/byemail?key=%%22%@%%22"

//#define NEW_URL @"http://127.0.0.1:5984/schedule/_design/views/_view/byemail?key=%%22%@%%22"

@interface Store : NSObject
-(void)fetchData:(NSString *)idOfSomeAddres block:(void (^)(NSDictionary *dictionary, NSError *err))block;

-(void)postData:(NSData *)data block:(void (^)(NSDictionary *dictionary, NSError *err))block;
-(NSData*)createJsonFromDictionary:(NSDictionary*)dictionary;

@end
