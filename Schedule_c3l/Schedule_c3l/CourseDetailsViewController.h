//
//  CourseDetailsViewController.h
//  Schedule_c3l
//
//  Created by susan on 10/31/12.
//  Copyright (c) 2012 Susan Sarabia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Course.h"
@interface CourseDetailsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property(nonatomic , strong)Course *selectedCourse;
@property (weak, nonatomic) IBOutlet UILabel *courseName;
@property (weak, nonatomic) IBOutlet UILabel *courseId;
@property (weak, nonatomic) IBOutlet UILabel *coursePoints;
@property (weak, nonatomic) IBOutlet UILabel *courseTeacher;
@property (weak, nonatomic) IBOutlet UILabel *courseDescription;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
