//
//  StudentDailyScheduleTableViewController.h
//  Schedule
//
//  Created by susan on 10/23/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CourseEvent.h"
@interface StudentDailyScheduleTableViewController : UITableViewController
@property(nonatomic)NSDate *selectedDate;
@property(nonatomic, strong)CourseEvent *selectedEvent;

- (IBAction)nextDay:(id)sender;
- (IBAction)prevDay:(id)sender;
-(void)changeDay:(int)dayToChange;

@end
