//
//  AdminEventEditViewController.h
//  Schedule
//
//  Created by susan on 10/18/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CourseEvent.h"
#import "Course.h"
@interface AdminEventEditViewController : UIViewController <UITextFieldDelegate>
@property(nonatomic, strong)Course *selectedCourse;
@property(nonatomic, strong)CourseEvent *selectedEvent;
@property(nonatomic, strong)CourseEvent *theOldEvent;

@property (weak, nonatomic) IBOutlet UITextField *classroomLabel;
@property (weak, nonatomic) IBOutlet UITextField *alternativeTeacherLabel;

@property (weak, nonatomic) IBOutlet UITextView *eventreadingInstructions;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UITextField *eventDescriptionTextField;
@property (weak, nonatomic) IBOutlet UIButton *startDateBtn;
@property (weak, nonatomic) IBOutlet UIButton *endDateBtn;
@property( nonatomic) BOOL isStartDateSelected;

@property(nonatomic)BOOL isNew;

- (IBAction)editStartDate:(id)sender;
- (IBAction)editEndDate:(id)sender;
- (IBAction)save:(id)sender;

@end
