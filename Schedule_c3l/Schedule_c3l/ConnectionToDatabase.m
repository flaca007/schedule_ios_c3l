//
//  ConnectionToDatabase.m
//  Schedule
//
//  Created by susan on 10/15/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import "ConnectionToDatabase.h"
static NSMutableArray *sharedConnectionList = nil;

@implementation ConnectionToDatabase
-(id)initWithRequest:(NSURLRequest *)req
{
    self = [super init];
    if(self){
        [self setRequest:req];
    }
    return self;
}
-(void)start
{
    //NSLog(@"start");
    //initialize container for data collected from nsurlconnection
    mutableData = [[NSMutableData alloc]init];
    
    //spawn connection
    internalConnection = [[NSURLConnection alloc]initWithRequest:[self request] delegate:self startImmediately:YES];
    
    //if this is the first connection started, create array
    if(!sharedConnectionList) sharedConnectionList = [[NSMutableArray alloc]init];
    
    //add the connection to the array so it doesnt get destroyed
    [sharedConnectionList addObject:self];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [mutableData appendData:data];
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
        
    NSDictionary *d = [NSJSONSerialization JSONObjectWithData:mutableData options:0 error:nil];

    if([self completionBlock]){
        [self completionBlock](d, nil);
        [sharedConnectionList removeObject:self];
    }
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    //pass the error from the connection to the completionblock
    if([self completionBlock]) [self completionBlock](nil, error);
    
    //destroy the connection
    [sharedConnectionList removeObject:self];
}
@end
