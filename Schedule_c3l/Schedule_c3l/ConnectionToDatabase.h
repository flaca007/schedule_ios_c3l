//
//  ConnectionToDatabase.h
//  Schedule
//
//  Created by susan on 10/15/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface ConnectionToDatabase : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate>
{
    NSURLConnection *internalConnection;
    NSMutableData *mutableData;
}
-(id)initWithRequest:(NSURLRequest *)req;
@property (nonatomic, copy)NSURLRequest *request;
@property(nonatomic, copy)void (^completionBlock)(id obj, NSError *err);

-(void)start;
@end
