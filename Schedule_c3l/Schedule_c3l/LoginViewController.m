//
//  LoginViewController.m
//  Schedule_c3l
//
//  Created by susan on 10/31/12.
//  Copyright (c) 2012 Susan Sarabia. All rights reserved.
//

#import "LoginViewController.h"
#import "UserStore.h"
#import "CourseListTableViewController.h"
@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.password.secureTextEntry = YES;
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)login:(id)sender
{
    if((self.userName.text.length == 0 && self.password.text.length == 0)){
        NSLog(@"wrong: %@ or password", self.userName.text);
    }else{
        //NSLog(@"right: %@", self.userNameText.text);
        
        [[UserStore sharedStore]logInUser:self.userName.text password:self.password.text callback:^{
            User *user = [[UserStore sharedStore]activeUser];
            //NSLog(@"user from mainview: %@", user);
            if([[UserStore sharedStore]activeUser]){
                if([[user userRole] isEqualToString:@"Student"]){
                    NSLog(@"I'm a student segue me to my storyboard");
                    
                    [self performSelectorOnMainThread:@selector(goToStudentHomePage) withObject:nil waitUntilDone:YES];
                    
                }else{
                    //get all users, get all courses for admin
                    NSLog(@"Im the boss");
                    [self performSelectorOnMainThread:@selector(goToAdminHomePage) withObject:nil waitUntilDone:YES];
                }
            }
        }];
        
    }
}

-(void)goToStudentHomePage
{
    [self performSegueWithIdentifier:@"toStudentHomePage" sender:self];
}
-(void)goToAdminHomePage


{
    [self performSegueWithIdentifier:@"toAdminHomePage" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"toAdminHomePage"]){
        CourseListTableViewController *courseListTableViewController = [segue destinationViewController];
        [courseListTableViewController setIsLoaded:@"NO"];
    }
    
}
@end
