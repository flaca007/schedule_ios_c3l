//
//  StudentMessagesTableViewController.h
//  Schedule
//
//  Created by susan on 10/25/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StudentMessagesTableViewController : UITableViewController
@property(nonatomic)NSArray *messages;
@end
