//
//  UserStorage.m
//  Schedule
//
//  Created by susan on 10/10/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import "UserStore.h"
#import "User.h"
#import "ConnectionToDatabase.h"
#import "CourseStore.h"
#import "Message.h"
@implementation UserStore


+(UserStore *)sharedStore
{
    static UserStore *sharedStore;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        
        sharedStore = [[self alloc]init];
        sharedStore->allUsers = [NSMutableArray array];
        sharedStore->activeUser = [[User alloc]init];
        sharedStore->userData = [NSDictionary dictionary];
        
    });
    return sharedStore;
}
-(id)init
{
    self = [super init];
    if(self){
        allUsers = [[NSMutableArray alloc]init];
        activeUser = nil;
    }
    return self;
}
-(void)setActiveUser:(User *)user
{
    activeUser = user;
    
}
-(NSDictionary *)userData
{
    return userData;
}
-(void)setUserdataDictionary:(NSMutableDictionary *)dictionary
{
    userData = dictionary;
}
-(User *)activeUser
{
    return activeUser;
}
-(NSArray *)allUsers
{
    return allUsers;
}
-(void)createUser:(NSDictionary *)userDictionary
{
    User *tempUser = [User userFromDictionary:userDictionary];
    NSLog(@"newly created user from admin: %@", tempUser);
    [allUsers addObject:tempUser];

}
-(void)logInUser:(NSString *)username password:(NSString *)password callback:(void (^)(void))callback
{
    NSString *urlExtensionWithUserId = [NSString stringWithFormat:@"_design/views/_view/login?key=[%%22%@%%22,%@]", username, password];
    //http://127.0.0.1:5984/schedule/_design/views/_view/login?key=["flaca007@gmail.com",123456]
    [self fetchData:urlExtensionWithUserId block:^(NSDictionary *dictionary, NSError *err) {
        
        NSMutableDictionary *userDictionaryData = [NSDictionary dictionary];
        //NSLog(@"dictionary: %@", dictionary);
        NSArray *userArray = [dictionary  valueForKey:@"rows"];
        userDictionaryData = [[userArray objectAtIndex:0] objectForKey:@"value"];
        User *loginUser = [User userFromDictionary:userDictionaryData];
        [[UserStore sharedStore]setActiveUser:loginUser];
        [[UserStore sharedStore]setUserdataDictionary:userDictionaryData];
        
        callback();
    }];
    
    
}

-(void)logInUser:(NSString *)userId callback:(void (^)(void))callback
{
    NSString *urlExtensionWithUserId = [NSString stringWithFormat:@"_design/views/_view/byemail?key=%%22%@%%22", userId];

    [self fetchData:urlExtensionWithUserId block:^(NSDictionary *dictionary, NSError *err) {
        
        NSMutableDictionary *userDictionaryData = [NSDictionary dictionary];
        //NSLog(@"dictionary: %@", dictionary);
        NSArray *userArray = [dictionary  valueForKey:@"rows"];
        userDictionaryData = [[userArray objectAtIndex:0] objectForKey:@"value"];
        User *loginUser = [User userFromDictionary:userDictionaryData];
        [[UserStore sharedStore]setActiveUser:loginUser];
        [[UserStore sharedStore]setUserdataDictionary:userDictionaryData];
        
        callback();
    }];
    
    
}

-(void)getActiveUserData:(void (^)(NSError *error))callback
{
    __block NSError *error = nil;
    
    //initiate tyhe request
    NSDictionary *dataForUser =  [[UserStore sharedStore]userData];
    //CourseService *courseService = [[CourseService alloc]init];
    for(NSString *courseId in [dataForUser valueForKey:@"studentCourses"]){
        [self fetchData:courseId block:^(NSDictionary *dictionary, NSError *err) {
            if(!err){
                NSLog(@"coursedictionary: %@", dictionary);
                Course *newCourse = [Course courseFromDictionaryWithEvents:dictionary];
                [[[UserStore sharedStore]activeUser]addCourseToUser:newCourse];
            }else{
                error = err;
                NSLog(@"error: %@", [error localizedDescription]);
            }
            callback(error);
        }];
        
    }
    
    NSString *urlWithMessageUserId = [NSString stringWithFormat:@"_design/views/_view/bymessages?key=%%22%@%%22" , [[[UserStore sharedStore]activeUser]db_id]];
    
    [self fetchData:urlWithMessageUserId block:^(NSDictionary *dictionary, NSError *err) {
        if(!err){
            NSMutableArray *messageArray = [NSMutableArray array];
            messageArray = [dictionary  valueForKey:@"rows"];
            

            
            for(NSDictionary *message in messageArray){
                //NSLog(@"the messages: %@",[message valueForKey:@"value"]);
                Message *tempMessage = [Message messageFromDictionary:[message valueForKey:@"value"]];
                [[[UserStore sharedStore]activeUser]addMessageToUser:tempMessage];
            }
            NSLog(@"active user: %@", [[UserStore sharedStore]activeUser]);
        }else{
            error = err;
            NSLog(@"error: %@", [error localizedDescription]);
        }
    }];    
}

@end
