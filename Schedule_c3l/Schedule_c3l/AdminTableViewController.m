//
//  AdminTableViewController.m
//  Schedule
//
//  Created by susan on 10/17/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import "AdminTableViewController.h"
#import "AdminStore.h"
#import "CourseStore.h"
#import "AdminCourseDetailViewController.h"
@interface AdminTableViewController ()
{
    
}

@end

@implementation AdminTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
//        NSLog(@"HÄR!!!!");
//        [self setIsLoaded:NO];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     NSLog(@"HÄR!!!!");
    //[self setIsLoaded:@"NO"];

    self.navigationController.toolbarHidden = false;
    
    NSLog(@"Is loaded:%@", self.isLoaded);
    if([[self isLoaded]isEqualToString:@"NO"]){
        [[AdminStore sharedStore]getAdminData:^(NSError *error) {
            [[self tableView]reloadData];
            
        }];
        //[self setIsLoaded:@"YES"];
       // NSLog(@"Is loaded again:%@", self.isLoaded);

    }

    // send message to the different classes to get all data
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    //NSLog(@"number of rows in section: %d", [[[CourseStore sharedStore]allCourses]count]);

    return [[[CourseStore sharedStore]allCourses]count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"adminCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Course *course = [[[CourseStore sharedStore] allCourses] objectAtIndex:[indexPath row]];
    
    [[cell textLabel]setText:[course courseName]];
    [[cell detailTextLabel]setText:[course courseTeacher]];
    
    return cell;
}
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"toAdminCourseDetail"])
    {
        NSIndexPath *indexPath=[self.tableView indexPathForSelectedRow];
        //NSLog(@"indexPath: %@", indexPath);
        
        //NSLog(@"XXXXX :%@",[[[CourseStore sharedStore] allCourses] objectAtIndex:[indexPath row]]);
        AdminCourseDetailViewController *adminDetailViewController = [segue destinationViewController];
        [adminDetailViewController setSelectedCourse:[[[CourseStore sharedStore] allCourses] objectAtIndex:[indexPath row]]];

    }
    if([segue.identifier isEqualToString:@"toStudentList"])
    {
        
    }
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
