//
//  AdminMessageViewController.h
//  Schedule
//
//  Created by susan on 10/22/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdminMessageViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *toLabel;
@property (weak, nonatomic) IBOutlet UITextField *titelTextField;
@property (weak, nonatomic) IBOutlet UITextView *messageTextView;
@property (nonatomic)NSArray *sendList;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

- (IBAction)sendMessage:(id)sender;

@end
