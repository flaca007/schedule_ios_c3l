//
//  StudentWeeklyReadingInstructionsTableViewController.h
//  Schedule
//
//  Created by susan on 10/23/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StudentWeeklyReadingInstructionsTableViewController : UITableViewController
@property(nonatomic)NSArray *eventArray;

@end
