//
//  DatePickerViewController.h
//  Schedule_c3l
//
//  Created by susan on 10/31/12.
//  Copyright (c) 2012 Susan Sarabia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Course.h"
#import "CourseEvent.h"
@interface DatePickerViewController : UIViewController
@property(nonatomic, strong)Course *selectedCourse;
@property(nonatomic, strong)CourseEvent *selectedEvent;
@property(nonatomic, strong)CourseEvent *theOldEvent;

@property( nonatomic) BOOL isStartDateSelected;

@property(nonatomic) NSDate *tempStartDate;
@property(nonatomic) NSDate *tempEndDate;

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (weak, nonatomic) IBOutlet UIButton *startDate;
@property (weak, nonatomic) IBOutlet UIButton *endDate;

- (IBAction)editStartDate:(id)sender;
- (IBAction)editEndDate:(id)sender;

-(void)datePickerToggleDate;
- (IBAction)save:(id)sender;

@end
