//
//  AdminStore.h
//  Schedule
//
//  Created by susan on 10/17/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import "Store.h"
#import "Message.h"
@interface AdminStore : Store
+(AdminStore *)sharedStore;

-(void)getAdminData:(void (^)(NSError *error))callback;

-(void)sendMessage:(Message *)message callback:(void (^)(NSError *error))callback;

@end
