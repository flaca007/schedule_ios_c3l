//
//  DatePickerViewController.m
//  Schedule_c3l
//
//  Created by susan on 10/31/12.
//  Copyright (c) 2012 Susan Sarabia. All rights reserved.
//

#import "DatePickerViewController.h"
#import "Utilities.h"
#import "CourseStore.h"
@interface DatePickerViewController ()

@end

@implementation DatePickerViewController
{
    Utilities *utilities;
    NSDateFormatter *formatter;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    utilities = [[Utilities alloc]init];
    formatter = [utilities dateFormatterWithDateTime];
    
    self.tempStartDate = [[self selectedEvent]eventStartDate];
    self.tempEndDate = [[self selectedEvent]eventEndDate];
    
    [[self datePicker]setDatePickerMode:UIDatePickerModeDateAndTime];
    [[self datePicker]setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT" ]];
    
    
    
    [self datePickerToggleDate];
    [[self datePicker]addTarget:self action:@selector(dateChange) forControlEvents:UIControlEventValueChanged];
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    [[self startDate]setTitle:[NSString stringWithFormat:@"Start - %@", [formatter stringFromDate:[self tempStartDate]]] forState:UIControlStateNormal];
    [[self endDate]setTitle:[NSString stringWithFormat:@"End - %@", [formatter stringFromDate:[self tempEndDate]]]  forState:UIControlStateNormal];
    
}
-(void)dateChange
{
    
    if([self isStartDateSelected]){
        self.tempStartDate =  self.datePicker.date;
        [[self startDate]setTitle:[NSString stringWithFormat:@"Start - %@", [formatter stringFromDate:[self tempStartDate]]] forState:UIControlStateNormal];
        
    }else{
        self.tempEndDate = self.datePicker.date;
        [[self endDate]setTitle:[NSString stringWithFormat:@"End - %@", [formatter stringFromDate:[self tempEndDate]]]  forState:UIControlStateNormal];
        
    }
}
-(void)datePickerToggleDate
{
    
    if([self isStartDateSelected]){
        [[self startDate]setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
        [[self datePicker]setDate:[self tempStartDate] animated:YES];
        [[self endDate]setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        
        // fix date picker set value , gives wrong date
        
    }else{
        [[self endDate]setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
        [[self startDate]setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        
        [[self datePicker]setDate:[self tempEndDate] animated:YES];
        
    }
    
}

- (IBAction)save:(id)sender
{
    [self setTheOldEvent:[self selectedEvent]];
    
    [[self selectedEvent]setEventStartDate:[self tempStartDate]];
    [[self selectedEvent]setEventEndDate:[self tempEndDate]];
    
    NSLog(@"selected course: %@", [self selectedCourse]);
    [[CourseStore sharedStore]saveCourse:[self selectedCourse] isNew:NO block:^(NSError *err) {
        //code
        NSLog(@"back in admin date picker view controller: %@", err);
        if(!err){
            [[self navigationController]popViewControllerAnimated:YES];
        }else{
            self.selectedEvent = self.theOldEvent;
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"ALERT" message:@"It did not go well" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
}
- (IBAction)editStartDate:(id)sender
{
    [self setIsStartDateSelected:YES];
    [self datePickerToggleDate];
}

- (IBAction)editEndDate:(id)sender
{
    [self setIsStartDateSelected:NO];
    [self datePickerToggleDate];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
