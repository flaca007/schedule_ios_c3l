//
//  AdminDatePickerViewController.h
//  Schedule
//
//  Created by susan on 10/18/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CourseEvent.h"
#import "Course.h"
@interface AdminDatePickerViewController : UIViewController

@property(nonatomic, strong)Course *selectedCourse;
@property(nonatomic, strong)CourseEvent *selectedEvent;
@property(nonatomic, strong)CourseEvent *theOldEvent;

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (weak, nonatomic) IBOutlet UIButton *startdateBtn;
@property (weak, nonatomic) IBOutlet UIButton *endDateBtn;
@property (weak, nonatomic) IBOutlet UIButton *editEnddate;

@property( nonatomic) BOOL isStartDateSelected;

@property(nonatomic) NSDate *tempStartDate;
@property(nonatomic) NSDate *tempEndDate;

- (IBAction)editStartDate:(id)sender;
- (IBAction)editEndDate:(id)sender;

-(void)datePickerToggleDate;

- (IBAction)saveDate:(id)sender;

@end
