//
//  CourseDetailsViewController.m
//  Schedule_c3l
//
//  Created by susan on 10/31/12.
//  Copyright (c) 2012 Susan Sarabia. All rights reserved.
//

#import "CourseDetailsViewController.h"
#import "Utilities.h"
#import "CourseEvent.h"
#import "Course.h"
#import "EventeditViewController.h"
@interface CourseDetailsViewController ()

@end

@implementation CourseDetailsViewController
{
    Utilities *utilities;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self tableView]reloadData];
    [[self courseName]setText:[[self selectedCourse]courseName]];
    [[self courseId]setText:[[self selectedCourse]courseId]];
    [[self coursePoints]setText:[[self selectedCourse]coursePoints]];
    [[self courseTeacher]setText:[[self selectedCourse]courseTeacher]];
    [[self courseDescription]setText:[[self selectedCourse]courseDescription]];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self selectedCourse]allEvents]count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"eventCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    //
    //    Course *course = [[[CourseStore sharedStore] allCourses] objectAtIndex:[indexPath row]];
    CourseEvent *event = [[[self selectedCourse]allEvents]objectAtIndex:[indexPath row]];
    utilities = [[Utilities alloc]init];
    NSDateFormatter *formatter = [utilities dateFormatterWithDateTime];
    NSDateFormatter *formatterTime = [utilities dateFormatterWithTime];
    
    //
    NSLog(@"cellforrowatindexpath.....");
    [[cell textLabel]setText:[event eventDescription]];
    [[cell detailTextLabel]setText:[NSString stringWithFormat:@"%@-%@",[formatter stringFromDate:[event eventStartDate]], [formatterTime stringFromDate:[event eventEndDate]]]];
    //    [[cell detailTextLabel]setText:[course courseTeacher]];
    
    return cell;
}
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    EventeditViewController *eventEditViewController = [segue destinationViewController];
    if ([segue.identifier isEqualToString:@"toEditEvent"])
    {
        NSIndexPath *indexPath=[[self tableView]indexPathForSelectedRow];
        
        
        
        [eventEditViewController setSelectedEvent:[[[self selectedCourse]allEvents]objectAtIndex:[indexPath row]]];
        [eventEditViewController setSelectedCourse:[self selectedCourse]];
    }
    if([segue.identifier isEqualToString:@"toAddNewCourseEvent"])
    {
        //CourseEvent *newEvent = [[CourseEvent alloc]init];
        //AdminEventEditViewController *adminEventEditViewController = [segue destinationViewController];
        [eventEditViewController setSelectedCourse:[self selectedCourse]];
        //[adminEventEditViewController setSelectedEvent: newEvent];
    }
    
}
@end
