//
//  CourseStorage.h
//  Schedule
//
//  Created by susan on 10/11/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Course.h"
#import "Store.h"

//#define BASE_URL @"http://127.0.0.1:5984/schedule/"

@interface CourseStore : Store
{
    NSMutableArray *allCourses;
}
-(NSArray *)allCourses;

+(CourseStore *)sharedStore;
-(Course *)createCourseWithEvents:(NSDictionary *)courseDictionary;
-(void)saveCourse:(Course *)course isNew:(BOOL)isNew block:(void (^)(NSError *err))block;


@end
