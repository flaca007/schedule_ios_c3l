//
//  AdminCourseDetailViewController.m
//  Schedule
//
//  Created by susan on 10/18/12.
//  Copyright (c) 2012 The A Team. All rights reserved.
//

#import "AdminCourseDetailViewController.h"
#import "CourseEvent.h"
#import "Course.h"
#import "AdminEventEditViewController.h"
#import "Utilities.h"
@interface AdminCourseDetailViewController ()

@end

@implementation AdminCourseDetailViewController
{
    Utilities *utilities;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[self tableView]reloadData];
    [[self courseNameLabel]setText:[[self selectedCourse]courseName]];
    [[self courseIdLabel]setText:[[self selectedCourse]courseId]];

	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self selectedCourse]allEvents]count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"adminCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
//    
//    Course *course = [[[CourseStore sharedStore] allCourses] objectAtIndex:[indexPath row]];
    CourseEvent *event = [[[self selectedCourse]allEvents]objectAtIndex:[indexPath row]];
    utilities = [[Utilities alloc]init];
    NSDateFormatter *formatter = [utilities dateFormatterWithDateTime];
    NSDateFormatter *formatterTime = [utilities dateFormatterWithTime];

//

    [[cell textLabel]setText:[NSString stringWithFormat:@"%@",[event eventDescription]]];
    [[cell detailTextLabel]setText:[NSString stringWithFormat:@"%@-%@",[formatter stringFromDate:[event eventStartDate]], [formatterTime stringFromDate:[event eventEndDate]]]];
//    [[cell detailTextLabel]setText:[course courseTeacher]];
    
    return cell;
}
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    AdminEventEditViewController *adminEventEditViewController = [segue destinationViewController];
    if ([segue.identifier isEqualToString:@"toAdminEditEvent"])
    {
        NSIndexPath *indexPath=[[self tableView]indexPathForSelectedRow];

        
        
        [adminEventEditViewController setSelectedEvent:[[[self selectedCourse]allEvents]objectAtIndex:[indexPath row]]];
        [adminEventEditViewController setSelectedCourse:[self selectedCourse]];
    }
    if([segue.identifier isEqualToString:@"toAddNewCourseEvent"])
    {
        //CourseEvent *newEvent = [[CourseEvent alloc]init];
        //AdminEventEditViewController *adminEventEditViewController = [segue destinationViewController];
        [adminEventEditViewController setSelectedCourse:[self selectedCourse]];
        //[adminEventEditViewController setSelectedEvent: newEvent];
    }
    
}
@end
